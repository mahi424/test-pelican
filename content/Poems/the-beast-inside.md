Title: The Beast Inside...

<pre>
I am living with this beast inside,
I try to tame it but it won't hide,.
I push it under my soul,
It always ripples outside.

I try hard not to let it out,
I keep it checked, tell none about,
It roars and scratch my inner self,
I just don't want others to listen it's shout.

Its peculiar how my only friend,
it assaults me, but saves me in the end.
It forces me out of my ways, make rules bend,
But it only does things to me to mend.

Life inside me is pretty tough,
I am a safe player while its bit rough,
I am diplomatic, It curls the puff,
He always wants more while i say enough.