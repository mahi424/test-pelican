Title: 99 YEARS OF LEASE...


<pre>
We all think of right times,
When we will be ready,
When legs wont be wobbly,
When life will be steady.

Some of us wait, 
Some start living.
Some hides under roof,
Some enjoys the raining.

One start today, 
One says tomorrow.
One go out in open and enjoy,
While one sleep in burrow.

One hold on to past,
One just let it go.
One keep crying the lost,
Other finds better as sun glow.

Some look for permanent,
Some look for ease.
Some afraid to live,
While some love, live and leave,
On a temporary 99 years of lease.
