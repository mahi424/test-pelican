Title: TELEPHONIC INTERVIEW...


<pre>
They said its at 2:30,
And i was ready at 2:10
Anxious and craming the last minute things,
My heart was about to burst,

When the clock tickked 2:30
I was scared but kind of ready.
As minutes were passing 
I was getting more and more anxious.

Unable to handle the stress
I lay down to calm my nerves.
The cool breeze from the fan were lulling me
As I felt the sleep in my eye
The phone rang to my surprise.

30 minutes late, but the call finally came.
The thick voice started asking
Tell me your introduction, 
and things you have done.

I was sleepy but I knew this chance's value.
I started replying all I knew
He was asking questions after questions
I felt like everything tailored for me.

The confidence started to boost 
with each question I was getting higher.
As he said thank you i felt so good
I thanked back and then thanked the lord.

I was thinking it would be tough and scary,
But It was really good and merry.
I hope this will get me somewhere better,
If not that I already got the experience that matter.