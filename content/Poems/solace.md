Title: Solace is inside..


<pre>
Wandering in the world with my pain,
Everyone is busy counting their loss and gain.
As I seek a shoulder to lay,
Everyone try harder to move away.
I wanna tell you how it all feel,
But all you care is money and meal.
Finding an ear is mammoth task,
Those who listens also wear mask.
When I reach a conclusion to me guide,
The only thing I get is that, 
Solace is inside..